import logging
import random
import time
import pyautogui
import config
import ctypes
import pyautogui._pyautogui_win
from pyautogui._pyautogui_win import _size

logging.getLogger(__name__)
try:
    import SendKeys as sk
except:
    pass

def _sendMouseEvent(ev, x, y, dwData=0):
    """The helper function that actually makes the call to the mouse_event()
    win32 function.

    Args:
      ev (int): The win32 code for the mouse event. Use one of the MOUSEEVENTF_*
      constants for this argument.
      x (int): The x position of the mouse event.
      y (int): The y position of the mouse event.
      dwData (int): The argument for mouse_event()'s dwData parameter. So far
        this is only used by mouse scrolling.

    Returns:
      None
    """
    assert x != None and y != None, 'x and y cannot be set to None'

    width, height = _size()
    convertedX = 65536 * x // width + 1
    convertedY = 65536 * y // height + 1
    ctypes.windll.user32.mouse_event(ev, ctypes.c_long(convertedX), ctypes.c_long(convertedY), dwData, 0)

pyautogui._pyautogui_win._sendMouseEvent = _sendMouseEvent


class Automation(object):
    def __init__(self, **configuration_keys):
        c = config.AutomationConfig.copy()
        c.update(configuration_keys)
        for k, v in c.iteritems():
            setattr(self, k, v)

    def delay(self, minimal=.0):
        time.sleep(
            max(
                minimal,
                self.ACTION_DELAY_MIN,
                random.random()*self.ACTION_DELAY_MAX
            )
        )

    def moveTo(self, box):
        logging.debug('MOVETO: %i, %i, %i, %i' % box.xywh)
        pyautogui.moveTo(
            box.x, box.y, duration=random.random()*self.ACTION_DELAY_MAX
        )
        self.delay()

    def mouse_down(self):
        pyautogui.mouseDown()

    def mouse_up(self):
        pyautogui.mouseUp()

    def click(self, position=None, repeats=1):
        if position:
            self.moveTo(position)
        for i in xrange(repeats):
            pyautogui.mouseDown()
            self.delay(.06)
            pyautogui.mouseUp()
        self.delay()

    def press(self, key, repeats=1, delay=0.25):
        for i in xrange(repeats):
            pyautogui.keyDown(key)
            self.delay(.025)
            pyautogui.keyUp(key)
            if i != repeats-1:
                time.sleep(delay)
        self.delay()

    def typewrite(self, message):
        pyautogui.typewrite(message,random.random()*self.TYPING_INTERVAL_MAX)

    def sendkeys(self, *args, **kwargs):
        kwargs['turn_off_numlock'] = False
        sk.SendKeys(*args, **kwargs)