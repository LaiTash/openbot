import os

import cv2
import numpy as np

from ffwf.captcha import normalize_symbols

if __name__ == '__main__':
    filenames = [f for f in os.listdir('captcha_samples')
                 if len(f.split('.'))==2]
    box = ''
    cols, rows = 100, 5
    start = 0
    blank = np.zeros((48 * rows, 64 * (cols), 3), np.uint8)
    blank[:] = [255, 255, 255]
    print len(filenames)
    for i, filename in enumerate(filenames[start:start+cols*rows]):
        row = i / cols
        col = i % cols
        filepath = os.path.join('captcha_samples', filename)
        img = cv2.imread(filepath)
        x = 64 * col
        y = 48 * row
        blank[y:y+24, x:x+48] = img
    cv2.imwrite('bdo.cpt.exp0.tif', blank)

