from ffwf.buyer import Buyer


class Solve(Buyer):
    def __init__(self, click_max=True):
        super(Solve, self).__init__()
        self.click_max = click_max

    def run(self):
        self.solve(self.click_max)



