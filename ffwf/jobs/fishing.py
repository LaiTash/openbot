from random import random
import time
from ffwf.fisherman import Fisherman
import logging


logging.getLogger(__name__)
try:
    import SendKeys as sk
except:
    pass
from ffwf.buyer import Buyer


class Fishing(Fisherman):
    should_reload = True

    def __init__(self, keep_loot_after=0):
        super(Fishing, self).__init__()
        self.mid_sum = 0
        self.mid_num = 0
        self.keep_loot_after = keep_loot_after

    def run(self):
        self.mid_sum = 0
        self.mid_num = 0
        while True:
            self.run_once()

    def _throw_pull_rod(self):
        while not self.detect_space_sign():
            pass
        logging.debug('Space sign detected, can throw a rod')
        logging.debug('Sending space keys')
        sk.SendKeys('   ', with_spaces=True)

    def run_once(self):
        self._throw_pull_rod()
        logging.debug('5 seconds delay')
        time.sleep(5)
        self._throw_pull_rod()
        logging.debug('Assuming a little delay')
        time.sleep(1.550)
        logging.debug('Sending space')
        sk.SendKeys('   ', with_spaces=True)
        logging.debug('Assuming 2.5 seconds delay')
        time.sleep(2.5)
        logging.debug('Solving minigame')
        presses = self.solve_minigame()
        logging.debug('Minigame solving result is: %s' % presses)
        sk.SendKeys(presses, .1 + random()*.15)
        logging.debug('Assuming 2-seconds delay')
        time.sleep(4)
        if len(presses) > self.keep_loot_after:
            logging.debug('Press r')
            sk.SendKeys('rrr', pause=min(.05, random()*0.2))
        else:
            logging.info('Will not keep this loot')
        logging.debug('Assuming 4-seconds delay')
        time.sleep(2)
        self.mid_sum += len(presses)
        self.mid_num += 1
        medium = self.mid_sum / self.mid_num
        logging.info(
            'Medium is: %i for %i fishing attempts' % (medium, self.mid_num)
        )