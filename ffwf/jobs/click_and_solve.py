from ..buyer import Buyer


class ClickAndSolve(Buyer):
    as_process = False

    def __init__(self, click_max=True):
        super(ClickAndSolve, self).__init__()
        self.click_max = click_max

    def run(self):
        self.click_and_solve(self.click_max)


