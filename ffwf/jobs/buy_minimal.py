import time
import pythoncom
from ffwf.buyer import Buyer
from ffwf.tools import IntervalTimer
import logging


logging.getLogger(__name__)


class BuyMinimal(Buyer):
    """ Buy at minimal price

    Parameters:
        int max-price: maximum price to buy
        int position: lot position, starting from 0
        bool click_max: click max button
        float refresh_interval: refresh interval
    """

    def __init__(
            self, max_price, position=0, click_max=True, refresh_interval=1.5,
            cheat=False, max_qty=0
    ):
        super(BuyMinimal, self).__init__()
        self.max_price = max_price
        self.position = position
        self.click_max = click_max
        self.refresh_interval = refresh_interval
        self.cheat = cheat
        self.cheat_status = 3
        self.max_qty = max_qty

    def run(self):
        logging.info('Starting buy-minimal job')
        while 1:
            price = self.expect_recognition(
                lambda : self.grab_first_price_lg()
            )
            if price is not None and price <= self.max_price:
                logging.debug('Found good item price')
                self.automation.click(self.positions.PRE_BUY_BUTTON)
                self.timer = IntervalTimer(self.refresh_interval)
                while 1:
                    price = self.expect_recognition(
                        lambda: self.grab_first_price_sm(self.position)
                    )
                    good_price = price is not None and price <= self.max_price
                    if good_price:
                        logging.debug('Found good lot price')
                        self.automation.moveTo(
                            self.positions.BUY_BUTTON(self.position)
                        )
                        self.try_solve()
                    else:
                        logging.debug('Will click back button')
                        self.cheat_status = 3
                        self.automation.click(self.positions.BACK_BUTTON)
                        break

    def try_solve(self):
        if self.cheat:
            self.try_solve_cheat()
        else:
            self.try_solve_nocheat()

    def try_solve_nocheat(self):
        try:
            self.click_and_solve(click_max=self.click_max,max_qty=self.max_qty)
        finally:
            if self.timer.passed():
                self._click_refresh()

    def try_solve_cheat(self):
        if not self.click_max:
            click_max = None
        elif self.cheat_status in (1, 2):
            click_max = self.click_max
        else:
            click_max = self.FIND_MAX
        try:
            result = self.click_and_solve(
                click_max=click_max, max_qty=self.max_qty
            )
            logging.debug('Result is %s' % result )
            if result:
                self.cheat_status += 1
                if self.cheat_status == 4:
                    self.cheat_status = 1
            else:
                self.cheat_status = 3
        finally:
            if self.timer.passed():
                self._click_refresh()

    def _click_refresh(self):
        logging.debug('Will click refresh button')
        self.automation.click(
            self.positions.REFRESH_BUTTON
        )
        time.sleep(0.1)