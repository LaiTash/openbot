import uuid

import config

import numpy as np
import os
import cv2
from thrd_party import pytesser
import Image
import timeit

last_uuid = str(uuid.uuid4())

MATCHING_TABLE = {
    'h': '4',
    't': '4',
    '[x': '4',
    'tx': '4',
    't.': '4',
    '?': '2',
    'L': '4',
    "'1": '7',
    "'l": '7',
    "'{": '7',
    "'I": '7',
    "'i": '7',
    "'[": '7',
    "`[": '7',
    "`{": '7',
    "`I": '7',
    "`i": '7',
    "'(": '7',
    'W': '7',
    'S': '8',
    'A': '4',
    "[3": '8',
    "o": '0',
    "O": '0',
    "C": '0',
    'G': '8',
    ';': '1',
    '{': '1',
    'i': '1',
    'I': '1',
    'T': '1',
    'a': '4',
    'Z': '2',

}


def extract_color(img, low, high):
    """ Extract specific color from image

    :param img: source image
    :param low: low HSV value
    :param high: high HSV value
    :return: resulting image
    """
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    high = np.array(high)
    low = np.array(low)
    mask = cv2.inRange(hsv, low, high)
    return mask, cv2.bitwise_and(img, img, mask=mask)


def threshold(img):
    """ cv2.threshold shortcut """
    ret, thresh = cv2.threshold(img, 255, 255, 255)
    return thresh


def find_contours(threshold, method=cv2.RETR_EXTERNAL):
    """ cv2.findContours shortcut """
    im2,contours,hierarchy = cv2.findContours(
        threshold, method,cv2.CHAIN_APPROX_NONE
    )
    return contours


def normalize_with_contour(img, contour):
    # Get symbol rotation
    if not len(contour):
        raise Exception('No contour')
    rect = cv2.minAreaRect(contour)
    (x, y), (w, h), theta = rect
    if h > w:
        theta -= 90

    # Create blank image
    bx, by, bw, bh = cv2.boundingRect(contour)
    blank = np.zeros((by+bh, bx+bw), np.uint8)
    blank[by:by+bh, bx:bx+bw] = img[by:by+bh, bx:bx+bw]

    # Rotate symbol
    matrix = cv2.getRotationMatrix2D((bx, by), theta-270, 1)
    blank = cv2.warpAffine(blank, matrix, (bx+bw+100, by+bh+100))

    # We need a bounding box again to crop the image correctly
    thresh = threshold(blank)

    contours = find_contours(thresh, cv2.RETR_EXTERNAL)
    merged = []
    for i in xrange(len(contours)):
        for j in xrange(len(contours[i])):
            merged.append(contours[i][j])
    contours = np.array(merged)

    bx, by, bw, bh = cv2.boundingRect(contours)
    blank = blank[by:by+bh, bx:bx+bw]

    # A small border around the symbol gives better OCR results
    blank = cv2.copyMakeBorder(
        blank,
        12-bh/2, 12-bh/2, 12-bw/2, 12-bw/2,
        cv2.BORDER_CONSTANT, value=[0, 0, 0]
    )
    #blank = cv2.resize(blank, (0,0), fx=1.7, fy=1.7)
    return 255-blank


def normalize_symbols(img):
    global last_uuid
    # Extract red pixels
    #mask, res = extract_color(img, [0, 150, 150], [10, 255, 255])
    img = cv2.resize(img, (0,0), fx=1.33, fy=1.33)
    mask, res = extract_color(img, [0, 150, 150], [25, 255, 255])

    # Create a greyscale image and create contours
    imgray = cv2.cvtColor(res,cv2.COLOR_BGR2GRAY)

    kernel = np.ones((3,3),np.uint8)
    dilation = cv2.dilate(imgray, kernel, iterations=1)

    thresh = threshold(dilation)

    contours = find_contours(thresh)

    # Merge nearby vectors and sort them as left-right
    merged_right, merged_left = [], []
    for i in xrange(len(contours)):
        for j in xrange(len(contours[i])):
            pt = contours[i][j]
            if pt[0][0] < 85:
                merged_left.append(pt)
            else:
                merged_right.append(pt)
    left = np.array(merged_left)
    right = np.array(merged_right)


    # Save captchas if config says so
    if config.SAVE_CAPTCHAS:
        last_uuid = str(uuid.uuid4())
        cv2.imwrite(
            os.path.join(config.SAVE_CAPTCHAS, '%s.original.bmp' % last_uuid),
            img
        )
        cv2.imwrite(
            os.path.join(config.SAVE_CAPTCHAS, '%s.mask.bmp' % last_uuid), mask
        )
        cv2.imwrite(
            os.path.join(config.SAVE_CAPTCHAS, '%s.dilate.bmp' % last_uuid),
            dilation
        )
        cv2.imwrite(
            os.path.join(config.SAVE_CAPTCHAS, '%s.thrsh.bmp' % last_uuid),
            thresh
        )
        ctrs = cv2.drawContours(img, [left, right], -1, (0,255,0), 1)
        rect = cv2.minAreaRect(left)
        box = cv2.boxPoints(rect)
        box = np.int0(box)
        cv2.drawContours(ctrs, [box], 0, (0, 0, 255), 1)
        rect = cv2.minAreaRect(right)
        box = cv2.boxPoints(rect)
        box = np.int0(box)
        cv2.drawContours(ctrs, [box], 0, (0, 0, 255), 1)

        cv2.imwrite(
            os.path.join(config.SAVE_CAPTCHAS, '%s.cntr.png' % last_uuid),
            ctrs
        )


    return normalize_with_contour(mask,left),normalize_with_contour(mask,right)


def analyze_symbol(symbol):
    img = Image.fromarray(symbol)
    result = pytesser.image_to_string(img).strip()
    match = MATCHING_TABLE.get(result, result)
    return str(match)


def analyze_captcha(img):
    normalized = normalize_symbols(img)
    blank = np.zeros((24, 48), np.uint8)
    blank[0:24, 0:24] = normalized[0][0:24, 0:24]
    blank[0:24, 24:48] = normalized[1][0:24, 0:24]
    if config.SAVE_CAPTCHAS is not None:
        cv2.imwrite(
            os.path.join(config.SAVE_CAPTCHAS, last_uuid+'.png'), blank
        )
    img = Image.fromarray(blank)
    result = pytesser.image_to_string(img, lang='bdo', psm='8').strip()
    result = result.replace(' ', '')
    #for key, value in MATCHING_TABLE.iteritems():
    #    if key in result:
    #        result = result.replace(key, value)
    return result


def analyze_captcha_bysymbol(img):
    normalized = normalize_symbols(img)
    return analyze_symbol(normalized[0]) + analyze_symbol(normalized[1])


def test_sanity():
    for filename in os.listdir('model'):
        filepath = os.path.join('model', filename)
        expects, ext = os.path.splitext(filename)
        #img = cv2.imread(filepath)
        img = Image.open(filepath).convert('RGB')
        img = cv2.cvtColor(np.array(img), cv2.COLOR_RGB2BGR)
        result = analyze_captcha(img)
        if not expects == result:
            print "ERROR RECOGNIZING '%s': result is '%s', '%s' expected" % (
                filename, result, expects
            )


if __name__ == '__main__':
    timer = timeit.Timer('test_sanity()', 'from __main__ import test_sanity')
    print 'WHOLE PROCESS TAKES', timer.timeit(1), 'seconds'