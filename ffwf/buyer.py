import re
import time

import Image
import cv2
import numpy as np

from automation import Automation
from ffwf import tools
from ffwf.botbase import Bot
from ffwf.tools import Box


import config
from errors import RecognitionError
from imagetools import grab_screenshot, crop
import positions
from thrd_party import pytesser
import captcha


class Buyer(Bot):
    should_reload = False
    FIND_MAX = 2
    CLICK_MAX = 1

    def _grab_and_match(self):
        self.img = grab_screenshot(self.positions.CAPTCHA_GRAB_BORDER)
        if self.save_img:
            cv2.imwrite('tempimage.png', self.img)
        min_val, max_val, self.buy_box = tools.match_template(
            self.img, positions.CAPTCHA_TEMPLATE
        )
        return min_val, max_val

    def get_qty_field(self):
        pbox = Box(*self.buy_box.top_left)
        qty_field_box =  pbox + self.positions.QTY_FIELD_OFFSET
        qty_field_img = qty_field_box.cv2_region(self.img)
        x, y = self.positions.QTY_WHITEPOINT_OFFSET.top_left
        try:
            if sum(qty_field_img[y, x]) == 765:
                return qty_field_box
        except:
            pass

    def get_qty(self):
        qty_field = self.get_qty_field()
        if not qty_field:
            return 1
        pbox = Box(*self.buy_box.top_left)
        qty_msg_box = pbox + self.positions.QTY_MESSAGE_OFFSET
        qty_msg_img = qty_msg_box.cv2_region(self.img)
        hsv = cv2.cvtColor(qty_msg_img, cv2.COLOR_BGR2HSV)
        low, high = [0, 150, 150], [25, 255, 255]
        high = np.array(high)
        low = np.array(low)
        mask = cv2.inRange(hsv, low, high)
        rs = cv2.bitwise_and(qty_msg_img, qty_msg_img, mask=mask)
        img = Image.fromarray(rs)
        result = re.sub('[^0-9]', '', pytesser.image_to_string(img, psm='8'))
        try:
            return int(result)
        except ValueError:
            return 1

    def input_qty(self, qty_box, max_qty, qty):
        #qty_box = self.get_qty_field()
        #if not qty_box:
        #    return None
        #max_qty = self.get_qty()
        click_position = Box(*self.positions.CAPTCHA_GRAB_BORDER.top_left)
        click_position = click_position + Box(*qty_box.top_left)
        click_position = click_position + self.positions.QTY_WHITEPOINT_OFFSET
        self.automation.click(click_position)
        l = len(str(max_qty))
        self.automation.mouse_down()
        if qty < 0:
            qty = max(1, max_qty+qty)
        else:
            qty = min(qty, max_qty)
        self.automation.typewrite('\x08'+str(qty))
        self.automation.mouse_up()
        self.automation.click(
            click_position+self.positions.QTY_CLOSE_OFFSET, repeats=2
        )
        pos = self.positions.CAPTCHA_GRAB_BORDER
        pos = pos + self.buy_box
        pos = pos + self.positions.CAPTCHA_INPUT_CLICK_OFFSET
        self.automation.click(pos)


    def _solve(self, press_enter=True):
        result = ''
        # Try to solve captcha until it looks right
        while (
            not (len(result) == 2 and result.isdigit())
            and (self.retry < self.num_retries or self.num_retries == 0)
        ):
            self.retry += 1
            self._grab_and_match()
            img = (self.buy_box + Box(w=-60)).cv2_region(self.img)
            result = self.solve_method(img)
        self.automation.typewrite(result)
        if press_enter:
            self.automation.press('enter')

    def click_and_solve(self, click_max=True, max_qty=0):
        self.automation.click()
        return self.solve(click_max=click_max, max_qty=max_qty)

    def find_max_button(self):
        self._grab_and_match()
        min_val, max_val, box = tools.match_template(
            self.img, positions.MAX_TEMPLATE
        )
        sx = self.positions.CAPTCHA_GRAB_BORDER.x
        sy = self.positions.CAPTCHA_GRAB_BORDER.y
        if box.y > self.buy_box.y:
            return None
        return Box(sx+box.x+10, sy+box.y+7)

    def solve(self, click_max=True, max_qty=0):
        do_click = True
        if int(click_max) < 0:
            self._grab_and_match()
            qty_box = self.get_qty_field()
            if qty_box:
                qty = self.get_qty()
                if qty < max_qty:
                    click_max = True
                else:
                    self.input_qty(qty_box, qty, click_max)
            else:
                click_max = False
        if click_max:
            self._grab_and_match()
            max_b = self.find_max_button()
            if max_b and click_max == self.CLICK_MAX:
                self.automation.moveTo(max_b)
        while self.retry < self.num_retries or self.num_retries == 0:
            try:
                self._solve(not (click_max==self.CLICK_MAX and max_b))
                if click_max == self.CLICK_MAX and max_b:
                    if do_click:
                        self.automation.click()
                        time.sleep(0.045)
                        do_click = False
                    self.automation.press('enter')
            except:
                return click_max and max_b


    def grab_price(self, box):
        try:
            img = grab_screenshot(box)
            img = cv2.resize(img, (0, 0), fx=3.0, fy=3.0)
            cv2.imwrite('img.png', img)
            img = Image.fromarray(img)
            result = pytesser.image_to_string(img)#.strip().replace(',', '')
            #result = result.replace(' ', '')
            result = re.sub('[^0-9]', '', result)
        except Exception as e:
            raise RecognitionError(str(e))
        if not (result and result.isdigit()):
            raise RecognitionError('Result is not digit')
        else:
            return int(result)

    def expect_recognition(self, fn, deadline=0.5):
        t = time.clock()
        while True:
            if time.clock() - t > deadline:
                return None
            try:
                return fn()
            except RecognitionError:
                continue

    def grab_first_price_lg(self):
        return self.grab_price(self.positions.FIRST_PRICE_BOX_LARGE)

    def grab_first_price_sm(self, lot=0):
        return self.grab_price(self.positions.LOT_PRICE_BOX(lot))

    def combine_offsets(self, frame, box_func, items_range):
        def_box = box_func(0)
        height = def_box.h
        first = items_range[0]
        n = items_range[1] - first
        img = np.zeros((height*n, def_box.w, 3),np.uint8)
        for i in xrange(*items_range):
            box = box_func(i)
            price_img = box.cv2_region(frame)
            img[height*(i-first):height*(i-first+1), :] = price_img
        return img

    def grab_min_prices(self, frame, items_range=(0, 7)):
        return self.combine_offsets(
            frame, self.positions.MIN_PRICE_BOX, items_range
        )

    def grab_lot_prices(self, frame, items_range=(0, 7)):
        return self.combine_offsets(
            frame, self.positions.LOT_PRICE_BOX, items_range
        )

    def grab_tradeitems_names(self, frame, items_range=(0, 7)):
        return self.combine_offsets(
            frame, self.positions.TRADEITEM_NAME_BOX, items_range
        )

    def get_tradeitems_info(self, frame, items_range=(0, 7)):
        names = self.grab_tradeitems_names(frame, items_range)
        prices = self.grab_min_prices(frame, items_range)
        names = pytesser.image_to_string(
            Image.fromarray(names), config.LANG, '1'
        ).split('\n\n')
        prices = pytesser.image_to_string(
            Image.fromarray(prices), 'eng', '6'
        ).split('\n')
        prices = [re.sub('[^0-9]', '', price) for price in prices]
        return zip(names, prices)

