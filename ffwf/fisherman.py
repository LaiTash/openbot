import Image
import ImageEnhance
import cv2
import numpy as np
from numpy.linalg import norm
import time
import operator
from ffwf import tools
from ffwf.imagetools import grab_screenshot
from ffwf.tools import match_template
from thrd_party.pytesser import pytesser
import positions
import logging
import re


logging.getLogger(__name__)


from ffwf.botbase import Bot
try:
    import SendKeys as sk
except:
    logging.info('Could not import Sendkeys, fishing will be disabled')


class Fisherman(Bot):
    def detect_space_sign(self):
        frame = grab_screenshot(self.positions.FISHING_SPACE_SIGN)
        img = Image.fromarray(frame)
        result = pytesser.image_to_string(img, 'eng', '8')
        return 'Space' in result

    def _count_maximums(self, img):
            max_v = 0
            final = -1
            h, w = img.shape[:2]
            for y in xrange(0, h):
                c_max_v = sum(img[y])
                if max_v <= c_max_v:
                    max_v = c_max_v
                    final = y
            return max_v, final


    def solve_minigame(self):
        frame = grab_screenshot(self.positions.FISHING_MINIGAME_APPROX )
        gs = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        ret, thresh = cv2.threshold(gs, 50, 255, 0)
        cv2.imwrite('thresh.png', thresh)
        c = cv2.cvtColor(thresh, cv2.COLOR_GRAY2BGR)
        minv, maxv, box = match_template(c, positions.FISHING_MINIGAME_TEMPLATE)
        frame = box.cv2_region(frame)
        cv2.imwrite('ext_frame.png', frame)
        frame = self.positions.FISHING_MINIGAME.cv2_region(frame)
        rects, result = self._extract_minigame_symbols(frame)
        img = result.copy()
        res = ''
        for rect in rects:
            X, Y, X1, Y1 = rect.xyx2y2
            cv2.rectangle(img, (X, Y), (X1, Y1), [255, 0, 0], 1)
            h = rect.h
            w = rect.w
            symbol = rect.cv2_region(result)
            y_val, y_max = self._count_maximums(symbol)
            logging.debug('y_max=%i, h=%i' % (y_max, h))
            if y_max < h/3:
                res += 's'
            elif y_max >= 2*h/3:
                res += 'w'
            else:
                symbol = symbol.swapaxes(0, 1)
                x_val, x_max = self._count_maximums(symbol)
                res += 'd' if x_max < w/2 else 'a'
        cv2.imwrite('boxes.png', img)
        return res

    def _extract_minigame_symbols(self, frame):
        def sumarr(arr1, arr2):
            return np.array([
                min(255, max(0, v1+v2)) for v1, v2 in zip(arr1, arr2)
            ])
        x, y = self.positions.FISHING_MINIGAME_SYMBOL(0).top_left
        frameHSV = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        hsv_value = frameHSV[y, x]
        hsv_value_low = sumarr(hsv_value, [-30, -150, -100])
        hsv_value_hi = sumarr(hsv_value, [30, 250, 250])

        result = cv2.inRange(frameHSV, hsv_value_low, hsv_value_hi)
        cv2.imwrite('frame.png', frame)
        cv2.imwrite('result.png', result)
        # result = 255-result
        ret, thresh = cv2.threshold(result, 255, 255, 255)
        im2, contours, hr = cv2.findContours(thresh, cv2.RETR_EXTERNAL,
                                             cv2.CHAIN_APPROX_NONE)
        rects = []
        for contour in contours:
            sx, sy, w, h = cv2.boundingRect(contour)
            if w + h < 15:
                continue
            rects.append(tools.Box(sx, sy, w, h))
        rects.sort(key=lambda r: r.x)
        return rects, result



