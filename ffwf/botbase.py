from collections import namedtuple
import win32gui
from automation import Automation
from ffwf import captcha
import config
from ffwf.tools import Box


def get_window_rect():
    result = [None]
    def _callback(hwnd, extra):
        title = win32gui.GetWindowText(hwnd)
        if not title.startswith('Black Desert'):
            return
        rect = win32gui.GetWindowRect(hwnd)
        result[0] = Box.init_xyx2y2(*rect)
    win32gui.EnumWindows(_callback, None)
    return result[0]


class Bot(object):
    as_process = True

    def __init__(self):
        self.positions = self._init_positions()
        self.tl, self.br, self.img = None, None, None
        self.solve_method = captcha.analyze_captcha
        self.save_img = False
        self.retry = -1
        self.num_retries = 0
        self.automation = Automation(**config.AutomationConfig)

    def _init_positions(self):
        base = config.POSITIONS.copy()
        rect = get_window_rect()
        diff = base.dimensions - rect
        base.move(diff.x-diff.w/2, diff.y-diff.h/2)
        return base

    def set_delays(self, **values):
        self.automation = Automation(**values)
        return self
