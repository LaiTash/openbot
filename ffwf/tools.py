import time
import cv2
import numpy as np

__author__ = 'Lai Tash'


def cv_size(img):
    return tuple(img.shape[1::-1])


def cv2_blit(x, y, target, source):
    target[y:, x:] = source


class Box(object):
    def __init__(self, x=0, y=0, w=0, h=0):
        self.xywh = x, y, w, h

    def copy(self):
        return self.__class__(*self.xywh)

    @classmethod
    def init_xyx2y2(cls, x, y, x2, y2):
        return cls(x, y, x2-x, y2-y)

    @property
    def xyx2y2(self):
        x, y, w, h = self.xywh
        return x, y, x+w, y+h

    def cv2_region(self, img):
        x, y, x2, y2 = self.xyx2y2
        return img[y:y2, x:x2]

    def __add__(self, other):
        result = [a+b for a, b in zip(self.xywh, other.xywh)]
        return self.__class__(*result)

    def __sub__(self, other):
        result = [a-b for a, b in zip(self.xywh, other.xywh)]
        return self.__class__(*result)


    @property
    def top_left(self):
        return tuple(self.xywh[:2])

    @property
    def x(self):
        return self.xywh[0]

    @property
    def y(self):
        return self.xywh[1]

    @property
    def x2(self):
        return self.x + self.w

    @property
    def y2(self):
        return self.y + self.h

    @property
    def w(self):
        return self.xywh[2]

    @property
    def h(self):
        return self.xywh[3]



class IntervalTimer(object):
    def __init__(self, interval):
        self.interval = interval
        self.started = time.clock()

    def reset(self):
        self.started = time.clock()

    def passed(self):
        current_clock = time.clock()
        result = current_clock - self.started > self.interval
        if result:
            self.started = current_clock
        return result


def match_template(img, template):
    h, w = template.shape[:-1]
    method = cv2.TM_SQDIFF
    res = cv2.matchTemplate(img, template, method)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
    top_left = min_loc
    return min_val, max_val, Box(top_left[0], top_left[1], w, h)