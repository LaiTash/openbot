import ImageGrab
import cv2
import numpy as np


def crop(img, x, y, w, h):
    return img[y:y+h, x:x+w]


def match_template(img, template):
    h, w = template.shape[:-1]
    method = cv2.TM_SQDIFF
    res = cv2.matchTemplate(img, template, method)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
    top_left = min_loc
    bottom_right = (top_left[0] + w, top_left[1] + h)
    return top_left, bottom_right


def grab_screenshot(bbox=None):
    printscreen_pil = ImageGrab.grab(bbox=(bbox.xyx2y2 if bbox else None))
    printscreen_pil = printscreen_pil.convert('RGB')
    return cv2.cvtColor(np.array(printscreen_pil), cv2.COLOR_RGB2BGR)