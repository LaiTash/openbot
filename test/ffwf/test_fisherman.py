import cv2
import pytest
import ffwf.fisherman as fm

def grab_screenshot_wr(img):
    img=img
    def grab_screenshot(box=None):
        if box:
            return box.cv2_region(img)
        else:
            return img
    return grab_screenshot


def test_detect_space_sign():
    fisherman = fm.Fisherman()
    img = cv2.imread('test/data/fisherman_space_sign.jpg')
    fm.grab_screenshot = grab_screenshot_wr(img)
    assert fisherman.detect_space_sign()

def test_solve_minigame():
    fisherman = fm.Fisherman()
    img = cv2.imread('test/data/fisherman_minigame.jpg')
    fm.grab_screenshot = grab_screenshot_wr(img)
    assert fisherman.solve_minigame() == 'SA'
