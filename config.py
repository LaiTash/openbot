import positions
import logging.config


logging.config.dictConfig({
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'converter': 'time.gmtimes',
            'format': '%(asctime)s [%(levelname)s]: %(message)s'
        }
    },
    'handlers': {
        'default': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'standard',
        },
        'tofile': {
            'level': 'DEBUG',
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'formatter': 'standard',
            'filename': 'log',
            'when': 'D'
        }
    },
    'loggers': {
        '': {
            'handlers': ['default', 'tofile'],
            'level': 'DEBUG',
            'propagate': True
        }
    }
})


OF_NOTHING, OF_ESC, OF_RETRY = 0, 1, 2
LG_ACCURATE, LG_FAST = 0, 1
ON_FAILURE = OF_RETRY
LANG = 'rus'
# Better don't touch this 3 lines
POSITIONS = positions.Positions1920x1080

# Set global delay randomisation intervals for all actions, might help you
# avoid being banned. Or not.
AutomationConfig = dict(
    TYPING_INTERVAL_MAX = 0.05,
    ACTION_DELAY_MIN = 0.005,
    ACTION_DELAY_MAX = 0.01,
)


SAVE_CAPTCHAS = None
#SAVE_CAPTCHAS = 'captcha_samples'  # Captcha saving mode is on
                                   # It is needed to collect captcha samples.
                                   # If you don't need/want to train
                                   # Tesseract OCR, comment this line


