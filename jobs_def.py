from ffwf.jobs.buy_minimal import BuyMinimal
from ffwf.jobs.click_and_solve import ClickAndSolve
from ffwf.jobs.fishing import Fishing
from ffwf.jobs.r_clicker import RClicker
from ffwf.jobs.solve import Solve
from ffwf.jobs.tests import TestOne

__author__ = 'Lai Tash'


def Ctrl1():
    """
    Will click the mouse, click max button, solve captcha, and press enter
    """
    return ClickAndSolve(click_max=True)


def Ctrl2():
    """
    Will click the mouse, solve captcha, and press enter. Does not click
    max button.
    """
    return ClickAndSolve(False)


def Ctrl3():
    """ Will solve currently opened captcha and press enter. """
    return TestOne()


def Ctrl4():
    """
    Will buy all items with any price, starting
    from first position, with pressing the "max" key
    """
    return BuyMinimal(
        float('inf'), position=0, click_max=-3, refresh_interval=9, cheat=True,
        max_qty=10
    ).set_delays(
        ACTION_DELAY_MIN=0.02, ACTION_DELAY_MAX=0.04
    )


def Ctrl5():
    """
    Will buy all items with price less or equal to 170000, starting from
    second position, without pressing the "max" key.
    """
    return BuyMinimal(180000, position=1, click_max=False).set_delays(
        ACTION_DELAY_MIN=0.02, ACTION_DELAY_MAX=0.04
    )


def Ctrl6():
    return BuyMinimal(
        float('inf'), position=0, click_max=True, refresh_interval=3,
        cheat=True
    ).set_delays(
        ACTION_DELAY_MIN=0.02, ACTION_DELAY_MAX=0.04
    )


def Ctrl7():
    return Fishing(keep_loot_after=3)


def Ctrl8():
    return RClicker()


def Ctrl9():
    pass