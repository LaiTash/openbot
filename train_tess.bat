tesseract %1.%2.exp%3.tif %1.%2.exp%3 batch.nochop makebox
c:\python27\python pyTesseractTrainer-1.03.py
tesseract %1.%2.exp%3.tif %1.%2.exp%3 nobatch box.train
unicharset_extractor %1.%2.exp%3.box
echo "%2 0 0 0 0 0" > ccc.font_properties
shapeclustering -F %1.font_properties -U unicharset %1.%2.exp%3.tr
mftraining -F %1.font_properties -U unicharset -O %1.unicharset %1.%2.exp%3.tr
cntraining %1.%2.exp%3.tr
move %1.%2.exp%3.box %1.box
move inttemp %1.inttemp
move pffmtable %1.inttemp
move normproto %1.normproto
move shapetable %1.shapetable
move %1.%2.exp%3.tr %1.tr
move unicharset %1.unicharset
combine_tessdata %1.