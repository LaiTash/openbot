import cv2
from ffwf.tools import Box


CAPTCHA_TEMPLATE = cv2.imread('templates/captcha.jpg')
MAX_TEMPLATE = cv2.imread('templates/max_button.png')
FISHING_MINIGAME_TEMPLATE = cv2.imread('templates/fishing_minigame.png')


Positions1920x1080_ = dict(
    CAPTCHA_GRAB_BORDER = Box(700, 450, 470, 280),
    FIRST_LOT_POSITION = Box(980, 385),
    #FIRST_PRICE_BOX_LARGE =  Box(865, 400, 125, 30),
    FIRST_PRICE_BOX_LARGE = Box(865, 402, 157, 26),
    PRE_BUY_BUTTON = Box(1327, 408),
    REFRESH_BUTTON = Box(1078, 768),
    BACK_BUTTON = Box(755, 770),
    FISHING_SPACE_SIGN = Box(915, 210, 87, 40),
    FISHING_MINIGAME_APPROX = Box(701, 293, 516, 199),
    FISHING_MINIGAME = Box(11, 35, 355, 18),
    _FISHING_MINIGAME_FST_SMB = Box(20,8),
    QTY_FIELD_OFFSET = Box(109, -69, 115, 28),
    QTY_MESSAGE_OFFSET = Box(-45, -101, 319, 28),
    QTY_WHITEPOINT_OFFSET = Box(57, 12),
    QTY_CLOSE_OFFSET = Box(184, 13),
    CAPTCHA_INPUT_CLICK_OFFSET = Box(204, 19),
    #FISHING_MINIGAME = Box(821, 355, 15, 18),

    _TRADEITEM_NAME_BOX = Box(729, 381, 147, 44),
    _BUY_BUTTON = Box(1315, 390),
    _MIN_PRICE_BOX = Box(865, 400, 125, 30),
    _LOT_PRICE_BOX = Box(1000, 390, 100, 30),

)

Positions1366x768_ = {
    key:value+Box(-272, -156) for key, value in Positions1920x1080_.iteritems()
}

class Positions:
    def __init__(self, dimensions, **kw):
        self.dimensions = dimensions
        self._positions = kw
        for k, v in kw.iteritems():
            setattr(self, k, v.copy())

    def copy(self):
        return Positions(self.dimensions, **self._positions)

    def move(self, x, y):
        for name, box in self.__dict__.iteritems():
            if isinstance(box, Box):
                setattr(self, name, box+Box(x, y))

    def BUY_BUTTON(self, position):
        return self._BUY_BUTTON + Box(y=position*60)

    def MIN_PRICE_BOX(self, position):
        return self._MIN_PRICE_BOX + Box(y=position*66)

    def LOT_PRICE_BOX(self, position):
        return self._LOT_PRICE_BOX + Box(y=position*60)

    def TRADEITEM_NAME_BOX(self, position):
        return self._TRADEITEM_NAME_BOX + Box(y=position*62)

    def FISHING_MINIGAME_SYMBOL(self, position):
        return (
            self._FISHING_MINIGAME_FST_SMB + Box(x=34*position)
        )

Positions1920x1080 = Positions(Box(0, 0, 1920, 1080), **Positions1920x1080_)
Positions1366x768 = Positions(Box(0, 0, 1366, 768), **Positions1366x768_)
