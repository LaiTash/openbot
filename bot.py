from thrd_party import pyhk
from multiprocessing import Process, Queue
import logging
import jobs_def


logging.getLogger(__name__)


class Bot(object):
    def __init__(self):
        self.current_job = None

    def run_command(self, command):
        if self.current_job is not None and self.current_job.exitcode is None:
            logging.error('Cannot start a new job before previous one is finished')
            return
        print 'starting'
        command = getattr(jobs_def, command)()
        if command.as_process:
            self.current_job = Process(target=command.run)
            self.current_job.start()
        else:
            command.run()

    def stop_job(self):
        if self.current_job is None or self.current_job.exitcode is not None:
            logging.info('Nothing to stop.')
            self.current_job = None
            return
        logging.info('STOPPING')
        self.current_job.terminate()
        self.current_job.join()
        self.current_job = None


class PyahkProcess(Process):
    def __init__(self, queue):
        super(PyahkProcess, self).__init__()
        self.queue = queue

    def command(self, command_n):
        self.queue.put(command_n)

    def run(self):
        hot = pyhk.pyhk()
        self.hotkeys = [
            hot.addHotkey(['Ctrl', '1'], lambda: self.command('Ctrl1')),
            hot.addHotkey(['Ctrl', '2'], lambda: self.command('Ctrl2')),
            hot.addHotkey(['Ctrl', '3'], lambda: self.command('Ctrl3')),
            hot.addHotkey(['Ctrl', '4'], lambda: self.command('Ctrl4')),
            hot.addHotkey(['Ctrl', '5'], lambda: self.command('Ctrl5')),
            hot.addHotkey(['Ctrl', '6'], lambda: self.command('Ctrl6')),
            hot.addHotkey(['Ctrl', '7'], lambda: self.command('Ctrl7')),
            hot.addHotkey(['Ctrl', '8'], lambda: self.command('Ctrl8')),
        ]
        hot.addHotkey(['Ctrl', '9'], lambda: self.command('stop'))
        hot.start()




hotkeys = []

if __name__ == '__main__':
    bot = Bot()
    queue = Queue()
    pyahk_process = PyahkProcess(queue)
    pyahk_process.start()
    while True:
        command = queue.get()
        if command == 'stop':
            bot.stop_job()
        else:
            bot.run_command(command)
